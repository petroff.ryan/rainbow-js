Architecture Design for Rainbow.chat JS  -- Research project//successor to https://gitlab.com/petroff.ryan/pflagchat


Humans involved
---------------

'Callers' are the users who come to Rainbow.chat to chat discreetly.
		Callers are greeted by a simple welcome screen and a button to chat now.
				Callers chat as they please.

'Operators' are the volunteers who have been trained by Pflag.
		Operators can log in with google accounts. Admins bless accounts before continuing.
				Operators chat with a Caller. 
				Operators may choose to open another chat with another Caller with a 'new tab' button.
					(New tab button can have the number of Callers waiting in the Callers_stack)

'Admins' are the Pflag overlords.
		Admins control access by 'blessing' Operators, allowing them to chat.

'Devs' are the creators of Rainbow.chat. They should not have a role in the rest of the story. 


Functional description
----------------------

Access Control: Operators and Admins log in with google at /operator and /admin. Admins bless Operators. Unblessed Operators wait.
Browsers ask the server for an image for a button, and the server returns a active/inactive image based on Operator activity


Callers who enter a chat are put in a Callers_stack, then attempt_chat_pairing()
Operators who log in or open a new tab are put in an Operators_stack, then attempt_chat_pairing() 
		When chats end, the Operator is returned to the Operators_stack, then attempt_chat_pairing()
		
		
		
Development plan
----------------

Start by implementing simplest server possible, using as little force as possible.
Once features are complete, then style it.

Start with yo webapp https://github.com/yeoman/generator-webapp
Add sockets and squlite
Make chat pairings happen

